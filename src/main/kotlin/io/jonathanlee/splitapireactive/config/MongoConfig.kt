package io.jonathanlee.splitapireactive.config

import com.mongodb.reactivestreams.client.MongoClient
import org.springframework.context.annotation.Configuration

@Configuration
class MongoConfig(private val mongoClient: MongoClient)
