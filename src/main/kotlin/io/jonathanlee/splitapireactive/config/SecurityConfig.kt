package io.jonathanlee.splitapireactive.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain

@Configuration
class SecurityConfig {

    @Bean
    fun securityWebFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain {
        return http
            .authorizeExchange()
            .pathMatchers("/**").permitAll()
            .pathMatchers("/register/**").permitAll()
            .pathMatchers("/login/**").permitAll()
            .pathMatchers("/user/**").permitAll()
            .pathMatchers("/v3/api-docs/**").permitAll()
            .pathMatchers("/swagger-ui/**").permitAll()
            .pathMatchers("/expense/**").hasAuthority("SCOPE_expense")
            .pathMatchers("/property/**").hasAuthority("SCOPE_property")
            .pathMatchers("/renter/**").hasAuthority("SCOPE_renter")
            .anyExchange().authenticated()
            .and()
            .cors()
            .and()
            .csrf().disable()
            .oauth2ResourceServer().jwt()
            .and()
            .and()
            .httpBasic().disable()
            .formLogin().disable()
            .build()
    }

}
