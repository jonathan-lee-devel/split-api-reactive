package io.jonathanlee.splitapireactive.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.net.URI

@RestController
@RequestMapping("/")
class IndexController {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    suspend fun getIndex(): ResponseEntity<Unit> {
        return ResponseEntity
            .status(HttpStatus.PERMANENT_REDIRECT)
            .location(URI.create("http://localhost:4200/login"))
            .build()
    }

}
