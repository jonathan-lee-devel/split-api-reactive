package io.jonathanlee.splitapireactive

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.web.reactive.config.EnableWebFlux

@EnableWebFluxSecurity
@EnableWebFlux
@SpringBootApplication
class SplitApiReactiveApplication

fun main(args: Array<String>) {
	runApplication<SplitApiReactiveApplication>(*args)
}
